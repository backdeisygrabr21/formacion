package net.techu.data;

import net.techu.Producto;
import org.springframework.data.mongodb.core.mapping.Document;


@Document("productosdeisy")
public class ProductoMongo {
    public String id;
    public String nombre;
    public String precio;
    public String color;

    public ProductoMongo() {

    }

    public ProductoMongo(String nombre, String precio, String color) {
        this.nombre = nombre;
        this.precio = precio;
        this.color = color;
    }
    @Override
    public String toString(){
        return String.format("Producto [id=%s, nombre=%s, precio=%s, color=%s]", id, nombre, precio, color);
    }
}
