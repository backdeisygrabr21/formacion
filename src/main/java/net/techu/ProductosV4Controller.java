package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProductosV4Controller {
    @Autowired
    private ProductoRepository repository;

    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado() {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }

    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        List<ProductoMongo> resultado = repository.findByNombre(nombre);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /*Obtener un rango de precio*/
    @GetMapping(value="/v4/productosbyprecio/{minimo}/{maximo}")
    public ResponseEntity<List<ProductoMongo>> obtenerProductosPorPrecio(@PathVariable double minimo, @PathVariable double maximo)
    {
        List<ProductoMongo> resultado = repository.findByPrecio(minimo, maximo);
        return new ResponseEntity<List<ProductoMongo>>(resultado, HttpStatus.OK);
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo) {
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    /* Modificar nombre y precio de los productos */
    @PutMapping("/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo) {
        Optional<ProductoMongo> resultado = repository.findById(String.valueOf(id));
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            resultado.get().nombre = productoMongo.nombre;
            resultado.get().color = productoMongo.color;
            resultado.get().precio = productoMongo.precio;
            ProductoMongo guardado = repository.save(resultado.get());
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        /*Posibilidad de buscar el producto, despues eliminarlo si exste*/
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
}
